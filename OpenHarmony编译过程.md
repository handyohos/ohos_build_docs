# OpenHarmony编译过程分析

## 1. 简介



## 2. 编译入口过程分析

### 2.1 build.sh脚本

- check_shell_environment检查系统shell命令

- export SOURCE_ROOT_DIR，检查.gn文件是否存在

- 根据uname设置HOST_DIR, HOST_OS, NODE_PLATFORM

  | 名称   | HOST_DIR   | HOST_OS | NODE_PLATFORM |
  | ------ | ---------- | ------- | ------------- |
  | Darwin | darwin-x86 | mac     | darwin-x86    |
  | Linux  | linux-x86  | linux   | linux-x64     |

- export工具相关环境变量

  | 名称        | 值                                                           |
  | ----------- | ------------------------------------------------------------ |
  | PYTHON3_DIR | ${SOURCE_ROOT_DIR}/prebuilts/python/${HOST_DIR}/current/     |
  | PYTHON3     | ${PYTHON3_DIR}/bin/python3                                   |
  | PYTHON      | ${PYTHON3_DIR}/bin/python                                    |
  | NODE_HOME   | ${SOURCE_ROOT_DIR}/prebuilts/build-tools/common/nodejs/node-v${EXPECTED_NODE_VERSION}-${NODE_PLATFORM} |

- export PATH环境变量

  | 路径                                                         | 说明 |
  | ------------------------------------------------------------ | ---- |
  | ${SOURCE_ROOT_DIR}/prebuilts/build-tools/${HOST_DIR}/bin     |      |
  | ${PYTHON3_DIR}/bin                                           |      |
  | ${SOURCE_ROOT_DIR}/prebuilts/build-tools/common/nodejs/node-v${EXPECTED_NODE_VERSION}-${NODE_PLATFORM}/bin |      |
  | ${SOURCE_ROOT_DIR}/prebuilts/build-tools/common/oh-command-line-tools/ohpm/bin |      |

- 设置npm配置

  | 路径           | 值                                           |
  | -------------- | -------------------------------------------- |
  | registry       | https://repo.huaweicloud.com/repository/npm/ |
  | @ohos:registry | https://repo.harmonyos.com/npm/              |
  | strict-ssl     | false                                        |
  | lockfile       | false                                        |

- init_ohpm：非ohos-sdk编译时才执行

  - 从contentcenter-vali-drcn.dbankcdn.cn下载ohcommandline-tools-linux.zip
  - ohpm config配置

  | 名称       | 值                               |
  | ---------- | -------------------------------- |
  | registry   | https://repo.harmonyos.com/ohpm/ |
  | strict_ssl | false                            |
  | log_level  | false                            |

  - 安装工具

  | 类型     | hvigor                                             | ohpm                                                         |
  | -------- | -------------------------------------------------- | ------------------------------------------------------------ |
  | 安装路径 | $HOME/.hvigor/wrapper/tools                        | $HOME/.ohpm                                                  |
  | 安装内容 | package.json: {"dependencies": {"pnpm": "7.30.0"}} | oh-package.json5: {"devDependencies":{"@ohos/hypium":"1.0.6"}} |
  | 安装命令 | npm install --silent                               | ohpm install                                                 |

- build_sdk：非ohos-sdk编译时才执行才执行，可通过--no-prebuilt-sdk或--prebuilt-sdk跳过

  - 编译产物目录：${ROOT_PATH}/prebuilts/ohos-sdk
  - 编译命令：./build.py --product-name ohos-sdk --load-test-config=false --get-warning-list=false --stat-ccache=false --compute-overlap-rate=false --deps-guard=false --generate-ninja-trace=false --gn-args skip_generate_module_list_file=true sdk_platform=linux ndk_platform=linux use_cfi=false use_thin_lto=false enable_lto_O0=true sdk_check_flag=false enable_ndk_doxygen=false archive_ndk=false sdk_for_hap_build=true

- tools_checker.py

  - check_os_version：Ubuntu 18.04, 20.04, 22.04
  - check_build_requried_packages：检查依赖的apt包

- 正式编译：${PYTHON3} ${SOURCE_ROOT_DIR}/build/hb/main.py build $args_list

### 2.2 hb编译过程

- hb路径： ~/.local/bin/hb
- hb内容：hb.__main__
- ~/.local/lib/python3.8/site-packages/hb/__main__.py
- 判断is_in_ohos_dir：逐级向上查询build/hb/resources/global_var.py文件是否存在
- 执行build/hb/main.py

## 3. hb编译过程分析

### 2.1 入口

```python
        main = Main()
        module_type = sys.argv[1]
        if module_type == 'build':
            module = main._init_build_module()
        elif module_type == 'set':
            module = main._init_set_module()
        elif module_type == 'env':
            module = main._init_env_module()
        elif module_type == 'clean':
            module = main._init_clean_module()
        elif module_type == 'tool':
            module = main._init_tool_module()
        try:
            module.run()
        except KeyboardInterrupt:
        		pass
```

__init_xxx_module

run()

### 2.2 build过程

```python
        args_dict = Arg.parse_all_args(ModuleType.BUILD)

        if args_dict.get("product_name").arg_value != '':
            set_args_dict = Arg.parse_all_args(ModuleType.SET)
            set_args_resolver = SetArgsResolver(set_args_dict)
            ohos_set_module = OHOSSetModule(set_args_dict, set_args_resolver, "")
            ohos_set_module.set_product()

        preloader = OHOSPreloader()
        loader = OHOSLoader()
        generate_ninja = Gn()
        ninja = Ninja()
        build_args_resolver = BuildArgsResolver(args_dict)

        return OHOSBuildModule(args_dict, build_args_resolver, preloader, loader, generate_ninja, ninja)
```

#### 2.2.1 Arg解析

读取build/hb/resources/args/default/xxxargs.json文件，结合sys.argv进行更新，更新后的值写入到out/hb_args/xxxargs.json文件中。内容示例：

```json
  ...
  "product_name": {
    "arg_name": "--product-name",
    "argDefault": "qemu-arm-linux-min",
    "arg_help": "Default:''. Help:Build a specified product. You could use this option like this: 1.'hb build --product-name rk3568@hihope' 2.'hb build --product-name rk3568'",
    "arg_phase": "prebuild",
    "arg_type": "str",
    "arg_attribute": {
      "abbreviation": "-p"
    },
    "resolve_function": "resolve_product",
    "testFunction": "testProduct"
  },
  ...
```

此时生产的out文件内容如下：

```sh
out/
├── hb_args
│   ├── buildargs.json
│   └── setargs.json
└── ohos_config.json
```

ohos_config.json的内容主要包括：

```json
{
  "root_path": "/home/handy/ohos",
  "board": "qemu-arm-linux",
  "kernel": null,
  "product": "qemu-arm-linux-min",
  "product_path": "/home/handy/ohos/vendor/ohemu/qemu_arm_linux_min",
  "device_path": "/home/handy/ohos/device/board/qemu/qemu-arm-linux",
  "device_company": "qemu",
  "os_level": "standard",
  "version": "3.0",
  "patch_cache": null,
  "product_json": "/home/handy/ohos/vendor/ohemu/qemu_arm_linux_min/config.json",
  "component_type": "",
  "product_config_path": "/home/handy/ohos/vendor/ohemu/qemu_arm_linux_min",
  "target_cpu": "arm",
  "target_os": "ohos",
  "out_path": "/home/handy/ohos/out/qemu-arm-linux",
  "subsystem_config_json": "build/subsystem_config.json",
  "device_config_path": "/home/handy/ohos/device/board/qemu/qemu-arm-linux",
  "support_cpu": null,
  "compile_config": null,
  "log_mode": "normal"
}
```



#### 2.2.2 build过程

如下所示，build分为以下几个阶段：

```python
    def run(self):
        try:
            self._prebuild()
            self._preload()
            self._load()
            self._pre_target_generate()
            self._target_generate()
            self._post_target_generate()
            self._pre_target_compilation()
            self._target_compilation()
        except OHOSException as exception:
            raise exception
        else:
            self._post_target_compilation()
        finally:
            self._post_build()
```

每个阶段都会调用_run_phase接口，该接口通过BuildArgsResolver解析处理每个args参数。

```python
    def _preload(self):
        self._run_phase(BuildPhase.PRE_LOAD)
        if self.args_dict.get('fast_rebuild', None) and not self.args_dict.get('fast_rebuild').arg_value:
            self.preloader.run()

    def _load(self):
        self._run_phase(BuildPhase.LOAD)
        if self.args_dict.get('fast_rebuild', None) and not self.args_dict.get('fast_rebuild').arg_value:
            self.loader.run()

    def _pre_target_generate(self):
        self._run_phase(BuildPhase.PRE_TARGET_GENERATE)

    def _target_generate(self):
        self._run_phase(BuildPhase.TARGET_GENERATE)
        if not self.args_dict.get("build_only_load").arg_value and not self.args_dict.get("fast_rebuild").arg_value:
            self.target_generator.run()

    def _run_phase(self, phase: BuildPhase):
        for phase_arg in [arg for arg in self.args_dict.values()if arg.arg_phase == phase]:
            self.args_resolver.resolve_arg(phase_arg, self)
```

_run_phase过程执行out/hb_args/buildargs.json中的命令，对应关系如下：

| 阶段                    | 内容示例              |                                  |
| ----------------------- | --------------------- | -------------------------------- |
| PRE_BUILD               | prebuild              |                                  |
| PRE_LOAD                | preload               |                                  |
| LOAD                    | load                  |                                  |
| PRE_TARGET_GENERATE     | preTargetGenerate     |                                  |
| TARGET_GENERATE         | targetGenerate        | gn过程                           |
| POST_TARGET_GENERATE    | postTargetGenerate    |                                  |
| PRE_TARGET_COMPILATION  | targetCompilation     | ninja过程                        |
| POST_TARGET_COMPILATION | postTargetCompilation | 编译成功后执行的命令             |
| POST_BUILD              | postbuild             | 无论成功失败，最后都会执行的命令 |

#### 2.2.3 preloader过程

preloader阶段生成如下中间文件：

```sh
out/
├── preloader
│   └── qemu-arm-linux-min
│       ├── build_config.json
│       ├── build_gnargs.prop
│       ├── build.prop
│       ├── compile_env_allowlist.json
│       ├── compile_standard_whitelist.json
│       ├── exclusion_modules.json
│       ├── features.json
│       ├── parts_config.json
│       ├── parts.json
│       ├── platforms.build
│       ├── subsystem_config.json
│       ├── syscap.json
│       └── SystemCapability.json
```



| 文件名                          | 内容示例                                                     | 说明                                                         |
| ------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| build_config.json               | {<br/>  "os_level": "standard",<br/>  "device_name": "qemu-arm-linux",<br/>  "product_company": "ohemu",<br/>  "product_name": "qemu-arm-linux-min",<br/>  "enable_ramdisk": true,<br/>  "support_jsapi": false,<br/>  "device_company": "qemu",<br/>  "target_os": "ohos",<br/>  "target_cpu": "arm",<br/>  "device_build_path": "device/qemu/arm_virt/linux",<br/>  "product_toolchain_label": "//build/toolchain/ohos:ohos_clang_arm"<br/>} | 全局的编译宏配置；gn阶段BUILDCONFIG.gn通过readfile加载为gn的全局宏 |
| build.prop                      | os_level=standard<br/>device_name=qemu-arm-linux<br/>product_company=ohemu<br/>product_name=qemu-arm-linux-min<br/>enable_ramdisk=True<br/>support_jsapi=False<br/>device_company=qemu<br/>target_os=ohos<br/>target_cpu=arm<br/>device_build_path=device/qemu/arm_virt/linux<br/>product_toolchain_label=//build/toolchain/ohos:ohos_clang_arm | json转化的nv对，哪里使用？建议删除                           |
| features.json                   | {<br/>  "features": {<br/>    "hichecker_support_asan": false,<br/>    "dsoftbus_get_devicename": false<br/>  },<br/>  "part_to_feature": {<br/>    "hichecker": [<br/>      "hichecker_support_asan"<br/>    ],<br/>    "dsoftbus": [<br/>      "dsoftbus_get_devicename"<br/>    ]<br/>  }<br/>} | 根据产品配置生成的各部件features配置；loader阶段使用         |
| build_gnargs.prop               | hichecker_support_asan=false<br>dsoftbus_get_devicename=false | features对应的nv对，哪里使用？待删除                         |
| compile_env_allowlist.json      | {<br> "ninja": []<br>}                                       | ninja编译阶段的环境变量；原始内容来源于build/compile_env_allowlist.json |
| compile_standard_whitelist.json | {<br> "bundle_subsystem_error": [<br>  "device/board/hihope/rk3568/ohos.build",<br>  ... | 编译问题白名单列表                                           |
| exclusion_modules.json          | {}                                                           | 产品各部件中需要忽略的模块，待删除？                         |
| parts_config.json               | {<br> "build_build_framework": true,<br> "startup_init": true, | 产品支持的部件列表；用于gn阶段判断当前产品是否支持某部件     |
| parts.json                      | {<br> "parts": [<br>  "build:build_framework                 | 产品支持的部件列表；loader阶段使用                           |
| platforms.build                 | {<br/>  "version": 2,<br/>  "platforms": {<br/>    "phone": {<br/>      "target_os": "ohos",<br/>      "target_cpu": "arm",<br/>      "toolchain": "//build/toolchain/ohos:ohos_clang_arm",<br/>      "parts_config": "parts.json"<br/>    }<br/>  }<br/>} | 平台列表；loader阶段使用。【疑问】为啥preloader.py中_generate_platforms_build生成此文件时使用固定的phone作为platform的名称？固定了有啥意义？如何支持多platforms？ |
| subsystem_config.json           | {<br> "arkui": {<br>  "path": "foundation/arkui",<br>  "name": "arkui"<br>  } | 子系统和路径表                                               |
| syscap.json                     | {<br> "syscap": {},<br> "part_to_syscap": {}<br>}            | 各部件的syscap                                               |
| SystemCapability.json           |                                                              |                                                              |

#### 2.2.4 loader过程

loader阶段生成如下中间文件：

```sh
out/
└── qemu-arm-linux
    ├── build_configs
    |   ├── BUILD.gn
    |   ├── inner_kits_list.gni
    |   ├── parts_list.gni
    |   ├── parts_test_list.gni
    |   ├── platforms_list.gni
    |   ├── system_kits_list.gni
    |   ├── auto_install_parts.json
    |   ├── parts_src_flag.json
    |   ├── required_parts_targets_list.json
    |   ├── component_override_map.json
    |   ├── phone_system_capabilities.json
    |   ├── target_platforms_parts.json
    |   ├── infos_for_testfwk.json
    |   ├── platforms_parts_by_src.json
    |   ├── parts_different_info.json
    |   ├── required_parts_targets.json
    |   ├── phone-stub
    │   │   ├── zframework_stub_exists.gni
    |   ├── phony_targets
    │   │   ├── BUILD.gn
    |   ├── platforms_info
    │   │   ├── all_parts.json
    │   │   └── toolchain_to_variant.json
    |   ├── subsystem_info
    │   │   ├── subsystem_build_config.json
    │   │   ├── src_subsystem_info.json
    │   │   └── no_src_subsystem_info.json
    |   ├── mini_adapter
    │   │   ├── ability.json
    │   │   ├── global.json
    │   │   ├── startup.json
    │   │   ├── ...
    |   ├── parts_info
    │   │   ├── components.json
    │   │   ├── hisysevent_configs.json
    │   │   ├── inner_kits_info.json
    │   │   ├── parts_deps.json
    │   │   ├── parts_info.json
    │   │   ├── parts_modules_info.json
    │   │   ├── parts_path_info.json
    │   │   ├── parts_targets.json
    │   │   ├── part_subsystem.json
    │   │   ├── parts_variants.json
    │   │   ├── path_to_parts.json
    │   │   ├── phony_target.json
    │   │   └── subsystem_parts.json
    │   ├── ability
    │   │   ├── ability_base
    │   │   │   └── BUILD.gn
    │   ├── ...
```

##### 2.2.4.1 build_configs脚本文件

| 文件名                                | 内容示例                                                     | 说明                                                         |
| ------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| BUILD.gn                              | import("${root_build_dir}/build_configs/parts_list.gni")<br/><br/>group("parts_list") {<br/>  deps = parts_list<br/>}<br/>... | 定义parts_list, inner_kits, system_kits, parts_test以及phony_list全局编译目标列表。 |
| inner_kits_list.gni                   | inner_kits_list = [<br/>  ...<br/>]                          | innerapi编译列表。                                           |
| parts_list.gni                        | parts_list = [<br/>  ...<br/>]                               | 部件编译列表。                                               |
| parts_test_list.gni                   | parts_test_list = [<br/>  ...<br/>]                          | 测试目标编译列表                                             |
| system_kits_list.gni                  | system_kits_list = [<br/>  ...<br/>]                         | 未使用，建议删除。                                           |
| platforms_list.gni                    | target_platform_list = [ "phone" ]<br/>kits_platform_list = [ "phone" ] | kits_platform_list无用，target_platform_list用于编译最终镜像。 |
| phony_targets/BUILD.gn                | group("part_phony_targets") {<br/>  deps = []<br/>}          | 空的phony_targets。                                          |
| phone-stub/zframework_stub_exists.gni | zframework_stub_exists = false                               | 未使用，删除？                                               |
| {sub_system}/{component}/BUILD.gn     | import("//build/ohos/ohos_kits.gni")<br/>import("//build/ohos/ohos_part.gni")<br/>import("//build/ohos/ohos_test.gni")<br/><br/>ohos_part("init") {<br/>  subsystem_name = "startup"<br/>  module_list = [<br/>    ...<br/>  ]<br/>  origin_name = "init"<br/>  variant = "phone"<br/>}<br/><br/>ohos_inner_kits("init_inner_kits") {<br/>  sdk_libs = [{<br/>      type = "so"<br/>      name = "//base/startup/init/interfaces/innerkits:libbegetutil"<br/>      header = {<br/>        header_files = []<br/>        header_base = "['//base/startup/init/interfaces/innerkits/include']"<br/>      }<br/>    },<br/>    ...<br/>  ]<br/>  part_name = "init"<br/>  origin_name = "init"<br/>  variant = "phone"<br/>}<br/><br/>ohos_part_test("init_test") {<br/>  testonly = true<br/>  test_packages = [ "//base/startup/init/test:testgroup" ]<br/>  deps = [ ":init" ]<br/>  part_name = "init"<br/>  subsystem_name = "startup"<br/>} | 每个部件的编译入口文件。                                     |

##### 2.2.4.2 build_configs json文件

| 文件名                           |      |      |
| -------------------------------- | ---- | ---- |
| auto_install_parts.json          |      |      |
| parts_src_flag.json              |      |      |
| required_parts_targets_list.json |      |      |
| component_override_map.json      |      |      |
| phone_system_capabilities.json   |      |      |
| target_platforms_parts.json      |      |      |
| infos_for_testfwk.json           |      |      |
| platforms_parts_by_src.json      |      |      |
| parts_different_info.json        |      |      |
| required_parts_targets.json      |      |      |

##### 2.2.4.3 platforms_info配置文件

| 文件名                    | 内容示例                                                     | 说明                      |
| ------------------------- | ------------------------------------------------------------ | ------------------------- |
| all_parts.json            | {<br/>  "phone": [<br/>    "build_framework",<br/>    "c_utils",<br/>    "ylong_runtime",<br/>    ...<br/>  ]<br/>} | 该平台的部件列表。        |
| toolchain_to_variant.json | {<br/>  "platform_toolchain": {<br/>    "phone": "//build/toolchain/ohos:ohos_clang_arm"<br/>  },<br/>  "toolchain_platform": {<br/>    "//build/toolchain/ohos:ohos_clang_arm": "phone"<br/>  }<br/>} | 平台和toolchain的对应关系 |

##### 2.2.4.4 mini_adapter配置文件

| 文件名       | 内容示例                                                     | 说明                 |
| ------------ | ------------------------------------------------------------ | -------------------- |
| startup.json | {<br/>  "parts": [<br/>    "init",<br/>    "hvb",<br/>    "appspawn",<br/>    "bootstrap_lite"<br/>  ]<br/>} | 每个子系统的部件列表 |
| ...          |                                                              |                      |

##### 2.2.4.5 subsystem_info配置文件

| 文件名                      | 内容示例 | 说明 |
| --------------------------- | -------- | ---- |
| subsystem_build_config.json |          |      |
| src_subsystem_info.json     |          |      |
| no_src_subsystem_info.json  |          |      |

##### 2.2.4.6 parts_info配置文件

| 文件名                  | 内容示例 | 说明 |
| ----------------------- | -------- | ---- |
| components.json         |          |      |
| hisysevent_configs.json |          |      |
| inner_kits_info.json    |          |      |
| parts_deps.json         |          |      |
| parts_info.json         |          |      |
| parts_modules_info.json |          |      |
| parts_path_info.json    |          |      |
| parts_targets.json      |          |      |
| part_subsystem.json     |          |      |
| parts_variants.json     |          |      |
| path_to_parts.json      |          |      |
| phony_target.json       |          |      |
| subsystem_parts.json    |          |      |

#### 2.2.5 gn过程

gn过程时的加载过程如下：

```sh
handy@handy-Parallels-Virtual-Platform:~/ohos$ gn gen out/qemu-arm-linux -v
Using source root /home/handy/ohos
Got dotfile /home/handy/ohos/.gn
Using build dir //out/qemu-arm-linux/
Loading //build/config/BUILDCONFIG.gn
Loading //build/core/gn/BUILD.gn
Running //build/core/gn/BUILD.gn with toolchain //build/toolchain/ohos:ohos_clang_arm
```

##### 2.2.5.1 【入口】dotgn

```go
import("//build/core/gn/ohos_exec_script_allowlist.gni")

# The location of the build configuration file.
buildconfig = "//build/config/BUILDCONFIG.gn"

# The source root location.
root = "//build/core/gn"

# The executable used to execute scripts in action and exec_script.
script_executable = "/usr/bin/env"

exec_script_whitelist = ohos_exec_script_config.exec_script_allowlist

# Enable OpenHarmony components for gn.
ohos_components_support = true
```

##### 2.2.5.2 【串行】BUILDCONFIG配置加载

全局配置，定义toolchain。

##### 2.2.5.3 【并行】BUILD.gn配置加载

make_all -> make_inner_kits, packages, images

##### 2.2.5.4 【并行】packages编译

对每个platform完成_install_modules命令。

```go
foreach(_platform, target_platform_list) {
    if (is_standard_system && !skip_gen_module_info) {
      # Lite system uses different packaging scheme, which is called in hb.
      # So skip install_modules for lite system since it's the packaging
      # action of standard system.

      deps += [ ":${_platform}_install_modules" ]
    }
    if (!skip_gen_module_info) {
      deps += [ ":gen_required_modules_${_platform}" ]
    }
    deps += [ ":${_platform}_parts_list" ]
  }
  if (make_osp) {
    deps += [ ":open_source_package" ]
  }
```





##### 2.2.5.5 【并行】images编译





#### 2.2.6 ninja过程



#### 2.2.7 post target过程



#### 2.2.7 post build过程



